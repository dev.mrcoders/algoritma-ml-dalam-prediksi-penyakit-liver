# ANALISA PERFORMA ALGORITMA MACHINE LEARNINGDALAM PREDIKSI PENYAKIT LIVER



### Description
Pada penelitian ini melakukan aanalisa  performa  algoritma  machine  learning  dengan  membandingkan  algortima  support  vector  machine, naïve bayes dan k-nearest neighbor.  Penelitian ini bertujuan untuk mengetahui performa algoritma apa yang paling tinggi accuracy pada data penyakit liver. Dari hasil penelitian dengan menggunakan splinting data 80:20 dapat  disimpulkan  bahwa  model  algoritma  Naïve  Bayes  memiliki  performa  lebih  baik  dari  pada  model algoritma  lainnya  ketika  menggunakan  teknik  SMOTE  dengan  nilai  accuracy  65,51%,  sedangkan  saat  tidak menggunakan  teknik  SMOTE  algoritma  Support  Vector  Machine  memiliki  performa  lebih  baik  dari  pada model algoritma lainnya dengan nilai accuracy pada data tidak 72.41%.